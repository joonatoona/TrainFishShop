"""Used to initialize everything"""

from datetime import datetime

import stripe

from flask import Flask, render_template, request
from flask_login import LoginManager, current_user

from trainfishshop.models import User, Product, File
from trainfishshop.database import init_db
from trainfishshop.config import _cfg
from trainfishshop.blueprints.html import HTML

# Setup flask
APP = Flask(__name__)
APP.secret_key = _cfg("Secret")
APP.register_blueprint(HTML)

# Setup flask_login
LOGIN_MANAGER = LoginManager()
LOGIN_MANAGER.init_app(APP)
LOGIN_MANAGER.anonymous_user = lambda: None

# Setup DB
init_db()

# Setup stripe
stripe.api_key = _cfg("StripeApiKey")


@LOGIN_MANAGER.user_loader
def load_user(uid: int):
    """Load a user by userid"""
    return User.query.filter(User.id == uid).first()


@APP.errorhandler(404)
def handle_404(err): #pylint: disable=unused-argument
    """Handle a 404 error"""
    return render_template("404.html"), 404


@APP.context_processor
def interject():
    """I'd just like to interject for moment. What you're refering to as Linux, is in fact,
    GNU/Linux, or as I've recently taken to calling it, GNU plus Linux."""
    return {
        "_c": _cfg,
        "_q": request.args,
        "_u": current_user,
        "_p": Product,
        "_f": File,
        "_d": datetime.utcnow().replace(microsecond=0).isoformat()
    }
