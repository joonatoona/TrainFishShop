"""Helpers for database"""

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

from trainfishshop.config import _cfg

ENGINE = create_engine(_cfg("DatabaseUrl"))
DB = scoped_session(sessionmaker(bind=ENGINE))

Base = declarative_base() #pylint: disable=invalid-name
Base.query = DB.query_property()

def init_db():
    """Initialize tables"""
    Base.metadata.create_all(bind=ENGINE)
    DB.commit() #pylint: disable=no-member
