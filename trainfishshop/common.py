"""Shared helper functions"""

from functools import wraps
from datetime import datetime
from hashlib import md5
from base64 import urlsafe_b64encode

from flask import redirect, flash
from flask_login import current_user

from trainfishshop.config import _cfg

def loginrequired(fun):
    """Require user to be logged in for route"""
    @wraps(fun)
    def wrapper(*args, **kwargs):
        """Wrapper function"""
        if not current_user:
            return redirect("/login")
        return fun(*args, **kwargs)
    return wrapper

def adminrequired(fun):
    """Require user to be admin for route"""
    @wraps(fun)
    def wrapper(*args, **kwargs):
        """Wrapper function"""
        if not current_user:
            return redirect("/login")
        elif not current_user.admin:
            flash("You do not have permission to access this page", "danger")
            return redirect("/account")
        return fun(*args, **kwargs)
    return wrapper

def securelink(target: str, addr: str, expires=86400):
    """Generate a link for nginx secure_link"""
    if not _cfg("DownloadSecure"):
        return target
    expires = int(datetime.utcnow().timestamp()) + expires
    secret = _cfg("Secret")
    s_hash = md5(f"{secret}+{target}.{addr}:{expires}".encode("UTF-8")).digest()
    s_hash = urlsafe_b64encode(s_hash).decode().rstrip("=")
    return f"{target}?h={s_hash}&e={expires}"
