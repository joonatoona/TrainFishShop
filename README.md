# TrainFishShop

Simple site to sell software

## Installation

```
virtualenv3 venv
source venv/bin/activate
pip install -r requirements.txt
```

## Configuration

Copy `config.ini.example` to `config.ini` and edit it

## Running

### Development

```
python app.py
```

### Production

```
gunicorn trainfishshop.app:app
```
